const { sum, del } = require('../src/sum')
var a = 2
test('adds 1+ 2 to equal 3', () => {
  console.log('inner 1')
  expect(sum(a, 2)).toBe(3)
  expect(del(a, 1)).toBeFalsy()
})
a = 1
console.log('outer')
test('adds 1+ 2 to equal 3', () => {
  console.log('inner 2')
  expect(sum(a, 2)).toBe(3)
  a = 4
  expect(del(a, 1)).toBe(3)
})

/** mock */
function forEach(items, callback) {
  for (let index = 0; index < items.length; index++) {
    callback(items[index])
  }
}
const mockCallback = jest.fn(x => 42 + x).mockName('mockFnc')
forEach([0, 1], mockCallback)
describe('mock test', () => {
  expect(mockCallback._isMockFunction).toBeTruthy()
  expect(mockCallback.mock.calls).toEqual([[0], [1]])
  expect(mockCallback.mock.calls[0][0]).toBe(0)
  expect(mockCallback.mock.results[0].value).toBe(42)
  mockCallback.mockReturnValue(100).mockReturnValueOnce(99)
  expect(mockCallback()).toBe(99)
  expect(mockCallback(111)).toBe(100)
  expect(mockCallback.getMockName()).toMatch(/mock/)
  expect(mockCallback).toBeCalled()
  expect(mockCallback).toBeCalledWith(1)
  expect(mockCallback).lastCalledWith(11111)
})
mockCallback
