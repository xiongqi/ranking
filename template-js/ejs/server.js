const http = require('http')
const ejs = require('ejs')
const port = 8080

http
  .createServer((req, res) => {
    let data = ''
    req.on('data', d => {
      data += d
    })
    req.on('end', () => {
      ejs.renderFile('./ejs/template.ejs', { name: 'xiongqi' }, (err, str) => {
        res.end(str)
      })
    })
  })
  .listen(port, () => {
    console.log('Server on', port)
  })
