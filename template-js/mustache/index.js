const Mustache = require('mustache')

const data = {
  name: '<xiongqi>',
  list: ['x','q']
}

const output = Mustache.render(`{{name}} {{#list}}\n{{.}}{{/list}}`, data)

console.log(output)
