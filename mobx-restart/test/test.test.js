import ShallowRenderer from 'react-test-renderer/shallow';
import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import { configure, mount, shallow, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import add from './src/add';

import AddTodo from '../src/components/Addtodo';

configure({ adapter: new Adapter() });
describe('Singal Test', () => {
  test('Add function', () => {
    expect(add(2, 3)).toBe(5);
  });
});

describe('Component Test', () => {
  test('AddTodo Test', () => {
    const renderer = new ShallowRenderer();
    renderer.render(<AddTodo />);
    const result = renderer.getRenderOutput();
    // console.log(result.props.children);
    expect(result.type).toBe('div');
    expect(result.props.children[1].type).toBe('button');
  });
  test('Button Click Test', () => {
    // const onClickMock = jest.fn();
    // const testInstance = ReactTestUtils.renderIntoDocument(<AddTodo testFunc={onClickMock}/>)
    // const buttonDom = ReactTestUtils.findRenderedDOMComponentWithTag(testInstance, 'div');
    // console.log(buttonDom);
    // ReactTestUtils.Simulate.click(buttonDom);
    // expect(onClickMock).toHaveBeenCalled();

    const onClickMock = jest.fn();
    const wrapper = shallow(<AddTodo testFunc={onClickMock}/>);
    wrapper.find('#testa').simulate('click');
    console.log(wrapper);
    expect(onClickMock.mock.calls.length).toBe(1);
  });
  test('Button Click Test 2', () => {
    const onClickMock = jest.fn();
    const wrapper = mount(<AddTodo all={{all: 1}} testFunc={onClickMock}/>);
    wrapper.find('#testa').simulate('click');
    // console.log(wrapper.props());
    expect(wrapper.props().all.all).toBe(2);
  });
  test('Button Test', () => {
    const onClickMock = jest.fn();
    const wrapper = render(<AddTodo />);
    expect(wrapper.text()).toBe('add');
    expect(wrapper.find('#testa').attr('id')).toBe('testa');
  });
});
