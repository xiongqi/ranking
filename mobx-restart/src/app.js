import React from 'react';
import { observable, autorun, computed } from 'mobx';
import { observer } from 'mobx-react';
import ReactDOM from 'react-dom';

const appState = observable({
  count: 0,
  get count2() {
    return `count: ${this.count}`;
  },
});
@observer
class Counter extends React.Component {
  render() {
    return (
      <button
        type="button"
        onClick={() => {
          this.props.appState.count += 1;
        }}
      >
        {this.props.appState.count}
      </button>
    );
  }
}
autorun(() => console.log(appState.count2));
ReactDOM.render(<Counter appState={appState} />, document.getElementById('main'));
