'use strict'; // eslint-disable-line
import React from 'react';
import { observable, action } from 'mobx';
import { observer } from 'mobx-react';
import ReactDOM from 'react-dom';
// import { Router, Route, Link, browserHistory as his } from 'react-router';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import AddTodo from './Addtodo';

const todoStore = observable({
  todos: [],
  get completed() {
    return this.todos.filter(x => x.isOk).length;
  },
});

@observer
export default class TodoList extends React.Component {
  @observable all = { all: 2 };

  render() {
    const { todos, completed } = this.props.todoStore;
    console.log('Todolist rendering...');
    return (
      <div>
        <h2>TodoList all: {this.all.all}</h2>
        <AddTodo todos={todos} all={this.all} />
        {todos.map(x => <Todo todo={x} key={x.task} />)}
        <div>completed: {completed}</div>
      </div>
    );
  }
}

function Todo(props) {
  const { todo } = props;
  console.log('Todo rendering...');
  return (
    <div>
      <input
        type="checkbox"
        checked={todo.isOk}
        onChange={action('toggleIsOk', () => {
          todo.isOk = !todo.isOk;
        })}
      />
      {todo.task}
    </div>
  );
}

// function AddTodo({ todos, all }) {
//   let task = '';
//   return (
//     <div>
//       <input
//         type="text"
//         placeholder="add....."
//         onChange={v => {
//           task = v.target.value;
//         }}
//       />
//       <button
//         onClick={action('add', () => {
//           todos.push({ isOk: false, task });
//           all.all += 1;
//         })}
//       >
//         add
//       </button>
//     </div>
//   );
// }

const Wrapper = () => <TodoList todoStore={todoStore} />;
const App = () => (
  <div>
    <div>App</div>
    <Link to="/todolist">todolist</Link>
    {/* <div>{props.children || ''}</div> */}
    <Route path="/todolist" component={Wrapper} />
  </div>
);
const RouterWrapper = () => (
  <Router>
    <Route path="/" component={App}>
      {/* <Route path="/todolist" component={Wrapper} /> */}
    </Route>
  </Router>
);
ReactDOM.render(<RouterWrapper />, document.getElementById('main'));

todoStore.todos.push({ task: 'todo1', isOk: true }, { task: 'todo2', isOk: false });
