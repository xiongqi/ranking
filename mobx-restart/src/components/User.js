import React from 'react';
import axios from 'axios';
import { userApi, searchApi } from '../data/githubApi';
import { observable, action, computed, useStrict } from 'mobx';
import { observer } from 'mobx-react';

useStrict(true);

class UserInfo {
  count = 5;
  @observable userData = '';
  // @computed get userName(){
  //   return this.userInfo.login || '1';
  // }
  // @computed get userId(){
  //   return this.userInfo.id || 1;
  // }
  @action
  getInfo = () => {
    console.log(++this.count);
    axios
      .get(userApi)
      .then(
        action(res => {
          this.userData = res.data;
        })
      )
      .catch(err => console.log('err:', err));
  };
  @action
  getInfo2 = () => {
    fetch(userApi)
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log('err:', err));
  };
}
const userInfo = new UserInfo();
@observer
export default class User extends React.Component {
  render() {
    return (
      <div>
        {this.props.appState.timer}
        <br />
        <button onClick={userInfo.getInfo}>get</button>
        <br />
        {JSON.stringify(userInfo.userData)}
        <br />
        {userInfo.userData.login}
        <br />
        {userInfo.userData.id}
        <br />
        {userInfo.count}
      </div>
    );
  }
}

const ins1 = new UserInfo();
const ins2 = new UserInfo();
console.log(ins1.count===ins2.count2);
debugger