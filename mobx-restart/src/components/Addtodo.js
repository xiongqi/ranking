import React from 'react';
import { action } from 'mobx';

export default function AddTodo({ todos = [], all = { all: 0 }, testFunc = () => {} }) {
  let task = '';
  function handleClick() {
    todos.push({ isOk: false, task });
    all.all += 1;
    testFunc();
  }
  return (
    <div>
      <input
        type="text"
        placeholder="add....."
        onChange={v => {
          task = v.target.value;
        }}
      />
      {/* <button onClick={handleClick}>add</button> */}
      <button id="testa" onClick={action('add', handleClick)}>
        add
      </button>
    </div>
  );
}
