const webpack = require('webpack');

module.exports = {
  // entry: './src/app.js',
  entry: './src/components/TodoList.js',
  output: {
    filename: 'bundle.js', // 打包文件名
    path: `${__dirname}/build`, // webpack本地打包路径,于publicPath作用不同

    // 运行服务器时的打包文件夹路径,即打包在 "http://网站根目录/dist/"下,通过"http://网站根目录/dist/bundle.js"访问.
    publicPath: '/dist/', // http://www.jb51.net/article/116443.htm  publicPath路径问题详解
  },
  // devtool: 'eval-source-map',
  devServer: {
    contentBase: './public', // webpack-dev-server提供本地服务器的文件夹
    inline: true, // 页面刷新模式.true: 内联刷新; false: iframe模式.
    historyApiFallback: false, // true 404路径都返回根index.html, 适用于单页应用;router时url错误能报错路由无法匹配.
    port: 8080,
    open: true,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader' },
          {
            loader: 'postcss-loader',
            options: {
              plugins: loader => [require('autoprefixer')(/* {browsers: ['last 5 versions']} */)],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    // new webpack.optimize.UglifyJsPlugin(),
  ]
};
