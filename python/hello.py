#!/user/bin/env python2
# -*-  coding: UTF-8 -*-
import time
print("hello")
p = '''string'''
print p
# raw_input("input\n")
a = b = c = 1
print b
print "name is %s %s" % ("xq", "a")

d = '12345'
print d[0:4:2]
print d[-5:]

list1 = [3, 33]
list2 = [4, 44]
print list1+list2*2

# 元组 只读列表 tuple
tuple1 = (1, '33', 4)
print tuple1*2

# 字典 类对象
dict1 = {'a': 1}
dict1['b'] = 2
print dict1, dict1['a'], dict1.keys(), dict1.values()

# 运算符
print 2**3, 4.6//3, -4//3, "a", "b"

# 条件语句
num = 3
if num <= 5 and num > 6:
    print num
elif num > 1:
    print 1
else:
    print 'else'

while num < 6:
    print num
    num += 1
else:  # 正常结束执行,非break跳出
    print num

for letter in 'test':
    print letter
else:  # 正常结束时执行
    print 'end'

print u"\u1F600"

print min(['a', '1'])

# list
print 'list'
print cmp([1], [2])

# time
localTime = time.localtime(time.time())
print localTime

# function

globalVar = 'globalVar'


def test(str, two, default=''):
    global globalVar # 访问全局变量
    globalVar = 'var'
    print str
    return str+two+default


print test('111', '2')  # 必备参数
print test(two="22", str='str')  # 关键字参数
print test(two="22", str='str', default="def")  # 默认参数
print globalVar


def test2(arg, *args):
    print arg, args


test2(1, 2, 3, 4)  # 不定长参数


def sum(a, b): return a + b  # 匿名函数


print sum(1, 2)
