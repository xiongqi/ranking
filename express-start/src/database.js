const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: 'test',
  database: 'test',
});
connection.connect(err => {
  if (err) console.log(err);
});
connection.query('SELECT * FROM dwd_orders', (err, results, fields) => {
  console.log('err', err);
  console.log('results', results);
  console.log('fields', fields);

  const date = new Date(results[1].date);
  console.log(date.toLocaleString());
});

const addSql = 'INSERT INTO dwd_orders(date, total, max, min) VALUES(CURDATE(), ?, ?, ?);';

const addData = [5000000, 200000, 1000];
connection.query(addSql, addData, (err, results, fields) => {
  console.log('err', err);
  console.log('results', results);
  console.log('fields', fields);
});
