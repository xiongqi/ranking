const path = require('path');

module.exports = {
  entry: { index: path.resolve(__dirname, './src/app.js') },
  output: {
    filename: '[name].js', // 打包文件名
    path: path.resolve(__dirname, './public'), // webpack本地打包路径,于publicPath作用不同

    // 运行服务器时的打包文件夹路径,即打包在 "http://网站根目录/dist/"下,通过"http://网站根目录/dist/bundle.js"访问.
    publicPath: '/', // http://www.jb51.net/article/116443.htm  publicPath路径问题详解
  },
  // devtool: 'eval-source-map',
  devServer: {
    contentBase: './public', // webpack-dev-server提供本地服务器的文件夹
    // inline: true, // 热重载
    historyApiFallback: true, // false 根据路径跳转页面 ; true 路径都返回根index.html
    port: 8080,
    open: true,
    // proxy: {
    //   '/stats/*':{
    //     target: 'http://127.0.0.1:50643',
    //     secure: false
    //   }
    // }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
};
