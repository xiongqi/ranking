const idbReuqest = window.indexedDB.open('test2', 1.1);
let db;
console.log('idbRequest', idbReuqest);
idbReuqest.onerror = event => console.log('error', event);
idbReuqest.onsuccess = event => {
  db = idbReuqest.result;
  console.log('success', idbReuqest.result);
  add(2);
  get(1);
};
idbReuqest.onupgradeneeded = event => {
  db = event.target.result;
  console.log('upgradedneeded', event.target.result);
  createStore('test2');
};

function createStore(name) {
  if (!db.objectStoreNames.contains(name)) {
    const objectStore = db.createObjectStore(name, { keyPath: 'id' });
    objectStore.createIndex('name', 'name', { unique: false }); // 建立索引
  }
}

function add(id) {
  const request = db
    .transaction(['test2'], 'readwrite')
    .objectStore('test2')
    .add({ id, name: 'xq1' });
  request.onsuccess = event => console.log('写入数据成功');
  request.onerror = event => console.log('写入数据失败');
}
function get(id) {
  const request = db
    .transaction(['test2'], 'readwrite')
    .objectStore('test2')
    .get(id);
  request.onsuccess = event => {
    console.log('读取数据成功');
    console.log(request.result);
  };
  request.onerror = event => console.log('读取数据失败');
}
db.transaction()
  .objectStore()
  .put(); // 更新数据
db.transaction()
  .objectStore()
  .delete(); // 删除数据
db.transaction()
  .objectStore()
  .delete(); // 删除数据
db.transaction()
  .objectStore()
  .index('name')
  .get('xq1'); // 索引查询
