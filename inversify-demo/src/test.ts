import 'reflect-metadata'
import { inject, injectable, Container } from 'inversify'

// @injectable()
class CodingTool {
  public open() {
    return 'opened'
  }
}

class Man {
  private codingTool: CodingTool
  constructor(codingTool: CodingTool) {
    this.codingTool = codingTool
  }
  public openTool() {
    return this.codingTool.open()
  }
}

let me = new Man(new CodingTool())

console.log(me.openTool())

// ------------------------------------------------------------------------------ //

// 将类转化为 injectable
@injectable()
class CodingTool2 {
  public open() {
    return 'vscode opened'
  }
}
@injectable()
class CodingTool3 {
  public open() {
    return 'txt opened'
  }
}

// 创建 DI Container
const container = new Container()
container.bind<CodingTool2>('CTool2').to(CodingTool3)

// 注入所需类
class Man2 {
  private codingTool: CodingTool2
  @inject('CTool2') private codingTool2: CodingTool2
  // constructor(@inject('CTool2') codingTool: CodingTool2) {
  //   this.codingTool = codingTool
  // }
  public openTool() {
    let ctool = container.get<CodingTool2>('CTool2')
    console.log(ctool)
    return ctool.open()
  }
}

const me2 = new Man2()
console.log(me2.openTool())
