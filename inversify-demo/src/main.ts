import { Container } from 'inversify'
import { Katana } from './entities'
import TYPES from './constants/identifiers'
import { Weapon } from './interfaces'

const container = new Container()

container.bind<Weapon>(TYPES.Weapon).to(Katana)

export default container
