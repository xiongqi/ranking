import { Warrior, Weapon, ThrowableWeapon } from '../interfaces'
import 'reflect-metadata'
import { injectable, inject } from 'inversify'

@injectable()
export class Katana implements Weapon {
  public hit() {
    return 'Katana hit!'
  }
}
