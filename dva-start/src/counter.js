import React from 'react';
import dva, { connect } from 'dva';
import { Router, Route } from 'dva/router';
import { delay } from 'dva/saga';
import createLoading from 'dva-loading';
import { Spin } from 'antd';

// Counter Component
function Counter({ dispatch, count, loading }) {
  function clickCount(type) {
    dispatch({ type })
  }
  return (
    <div style={{ textAlign: 'center' }}>
      <div >
        {count}
      </div>
      <button onClick={() => clickCount('count/add')}>add</button>
      <button onClick={() => clickCount('count/dec')}>dec</button>
      <button onClick={() => clickCount('count/addDelay')}>addDelay</button>
      <button onClick={() => clickCount('count/decDelay')}>decDelay</button>
      <button onClick={() => clickCount('count/addTenDelay')}>addTenDelay(takeLatest)</button>
      <button onClick={() => clickCount('count/decTenDelay')}>decTenDelay(takeEvery)</button>
      <br /><br />
      <div>
        <Spin size="large" spinning={loading.global} />
      </div>
    </div>
  )
}
// connect component
const ConnectedCounter = connect(({ count, loading }) => ({ count, loading }))(Counter);

// model
const counterModel = {
  namespace: 'count', // 全局 state 上的属性名
  state: 0,
  reducers: {
    add: count => count + 1,
    dec: count => count - 1,
    addTen: count => count + 10,
    decTen: count => count - 10,
  },
  effects: {
    *addDelay(action, effects) {
      yield effects.call(delay, 1000);
      yield effects.put({ type: 'add' });
    },
    *decDelay({ type }, { call, put }) {
      yield call(delay, 1000);
      yield put({ type: 'dec' })
    },
    addTenDelay: [
      function* (action, { call, put }) {
        yield call(delay, 1000);
        yield put({ type: 'addTen' })
      },
      { type: 'takeLatest' }
    ],
    decTenDelay: [
      function* (action, { call, put }) {
        yield call(delay, 1000);
        yield put({ type: 'decTen' })
      },
      { type: 'takeEvery' }
    ],
  }
}

// router
const router = ({ history }) => <Router history={history}>
  <Route path="/" exact component={ConnectedCounter} />
</Router>

// dva initialize
const app = new dva({
  initialState: {
    count: 1
  }
});

// plugins
app.use(createLoading());

// app.model
app.model(counterModel);

// app.router
app.router(router);

// app.start
app.start('#root');
