module.exports = {
  "entry": "src/counter.js",
  "devtool": "eval-source-map",
  "extraBabelPlugins": [
    [
      "import",
      {
        "libraryName": "antd",
        "libraryDirectory": "es",
        "style": "css"
      }
    ]
  ]
}
