import React, { Component } from 'react';
import LegacyContext from './LegacyContext';
import logo from './logo.svg';
import './App.css';

const TmpContext = React.createContext('default');
class App extends Component {
  render() {
    return (
      <div>
        <LegacyContext />
        <TmpContext.Provider value={{ a: 1 }}>
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">Welcome to React</h1>
            </header>
            <p className="App-intro">
              <Id />
            </p>
          </div>
        </TmpContext.Provider>
        <DefaultId />
      </div>
    );
  }
}
function Id(props) {
  return <TmpContext.Consumer>{tmp => <div>{tmp.a}</div>}</TmpContext.Consumer>;
}
function DefaultId() {
  return <TmpContext.Consumer>{tmp => <div>{tmp}</div>}</TmpContext.Consumer>;
}
export default App;
