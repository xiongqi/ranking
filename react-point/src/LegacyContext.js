import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Legacy extends Component {
  getChildContext() {
    return { color: 'red' };
  }
  render() {
    return (
      <div>
        <Button />
      </div>
    );
  }
}

Legacy.childContextTypes = {
  color: PropTypes.string,
};

class Button extends Component {
  render() {
    return <div>{this.context.color}</div>;
  }
}
Button.contextTypes = {
  color: PropTypes.string,
};
